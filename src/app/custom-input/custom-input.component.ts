import { Component, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-custom-input',
  templateUrl: './custom-input.component.html',
  styleUrl: './custom-input.component.scss',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CustomInputComponent),
      multi: true,
    },
  ],
})
export class CustomInputComponent implements ControlValueAccessor {
  private innerValue: string = '';

  private onTouchedCallback: () => void;
  private onChangeCallback: (_: string) => void;

  constructor() {
    this.onTouchedCallback = () => {};
    this.onChangeCallback = () => {};
  }

  get value() {
    return this.innerValue;
  }

  set value(va: string) {
    if (va !== this.innerValue) {
      this.innerValue = va;
      this.onChangeCallback(va);
    }
  }

  writeValue(value: any): void {
    if (value !== this.innerValue) {
      this.innerValue = value;
    }
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }
  onBlur() {
    this.onTouchedCallback();
  }
  setDisabledState?(isDisabled: boolean): void {}

  // onInput(event: Event): void {
  //   const inputElement = event.target as HTMLInputElement;
  //   this.value = inputElement.value;
  //   this.onTouchedCallback(this.value);
  //   this.onTouchedCallback();
  // }
}
