/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,ts}"],
  theme: {
    extend: {},
  },
  plugins: [],
  corePlugins: {
    // Enable only grid and flex utilities
    preflight: false, // Disable base styles
    container: false,
    float: false,
    objectFit: false,
    objectPosition: false,
    overflow: false,
    overscrollBehavior: false,
    position: false,
    inset: false,
    visibility: false,
    zIndex: false,

    // Active only flex and grid
    flex: true,
    flexDirection: true,
    flexWrap: true,
    placeContent: true,
    placeItems: true,
    placeSelf: true,
    alignContent: true,
    alignItems: true,
    alignSelf: true,
    justifyContent: true,
    justifyItems: true,
    justifySelf: true,
    order: true,
    gap: true,
    gridAutoFlow: true,
    gridTemplateColumns: true,
    gridColumn: true,
    gridColumnStart: true,
    gridColumnEnd: true,
    gridTemplateRows: true,
    gridRow: true,
    gridRowStart: true,
    gridRowEnd: true,
  },
};
